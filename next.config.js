const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');


module.exports = {
  webpack: (config) => {
    if (process.env.ANALYZE) {
      config.plugins.push(new BundleAnalyzerPlugin());
    }
    return config;
  }
};
