const express = require('express')
const next = require('next')
const console = require('console');
const compression = require('compression');

console.log(process.env.NODE_ENV);

const dev = process.env.NODE_ENV !== 'production'
var port = process.env.PORT || 3000;
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
.then(() => {
  const server = express();
  server.use(compression());

  // Common code for handling NEXT js Link queries.
  // server.get('/p/:id', (req, res) => {
  //   const actualPage = '/post';
  //   const queryParams = { id: req.params.id };
  //   app.render(req, res, actualPage, queryParams);
  // })


  // Code used for pulling pages Dynamicaly from content infrastructure... contentful
  // server.get('/blogposts', (req, res) => {
  //   const actualPage = '/BlogPosts';
  //   app.render(req, res, actualPage, {});
  // })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:'+port)
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})
