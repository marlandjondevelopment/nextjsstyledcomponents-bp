import React, { Fragment, Component } from "react";
import Head from 'next/head';

class HomePage extends Component {
  render() {
    return (
      <Fragment>
        <Head>
          <title>Home Page</title>
          <meta name="Description" content="Author: Carl Brown, Page: Home" />
        </Head>
        <h1>Entry to next js with styled components boiler plate</h1>
        <ul>
          <li>item 1</li>
          <li>item 2</li>
          <li>item 3</li>
        </ul>
      </Fragment>
    )
  }
}

export default HomePage;
